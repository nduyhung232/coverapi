# Sử dụng hình ảnh nền tảng Java 17
FROM openjdk:17-jdk-alpine

# Đặt thư mục làm việc
WORKDIR /app

# Sao chép file JAR từ máy chủ vào container
COPY build/libs/*.jar app.jar

# Cấu hình để container có thể mở cổng 8080
EXPOSE 8080

# Lệnh để chạy ứng dụng
ENTRYPOINT ["java", "-jar", "app.jar"]