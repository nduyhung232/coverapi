package com.example.coverapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@RestController
public class CoverApiController {
    private final String url = "http://103.226.248.48";
    private final String port = "8029";

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/{phone_number}/{brand}")
    public String callApi(@PathVariable String phone_number, @PathVariable String brand) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return restTemplate.exchange(
                String.format("%s:%s/callget.htm?Username=callget&password=AA1bNS22A&mobile=%s&brand=%s&content=ma%%20234"
                        , url, port, phone_number, brand), HttpMethod.GET, entity, String.class).getBody();
    }

    @GetMapping("/{key}")
    public String getApi(@PathVariable String key) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return restTemplate.exchange(
                String.format("%s:%s/getotp.htm?Username=callget&password=AA1bNS22A&mids=%s"
                        , url, port, key), HttpMethod.GET, entity, String.class).getBody();
    }
}
